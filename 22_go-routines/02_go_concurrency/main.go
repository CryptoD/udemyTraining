package main

import "fmt"

func main() {
	go foo()
	go bar()
}

func foo() {
	for i := 0; i < 15; i++ {
		fmt.Println("Foo:", i)
	}
}

func bar() {
	for i := 0; i < 15; i++ {
		fmt.Println("Bar:", i)
	}
}

// This one doesn't print anything. So something else has to be added.
// D.

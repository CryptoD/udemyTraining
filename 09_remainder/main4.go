package main

import "fmt"

func main() {

	for a := 1; a < 20; a++ {

		if a == 1 {
			fmt.Println("One")
		} else {
			fmt.Println("Other")
		}
	}
}
